import { Component } from '@angular/core';
import { Platform, Config, App, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../pages/tabs/tabs';
import { IntroPage } from '../pages/intro/intro';

// import * as fawryPlugin from './../assets/fawryPlugin.js';
// declare var  testao ;
// declare var fawryCallbackFunction:any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  myRoot: any;
  rootPage: any;
  get navCtrl(): NavController {
    return this.app.getRootNav();
  }
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private config: Config, private screenOrientation: ScreenOrientation,public storage:Storage,protected app: App) {
    platform.ready().then(() => {
      this.checkLoginDetails();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.config.set('backButtonText', '');
      if (platform.is('cordova')) {
        console.log(this.screenOrientation.type);
        screenOrientation.lock('portrait');
      }
    });
  }
  checkLoginDetails(){
    this.storage.get('myToken').then((val)=>{
      console.log('storage response for myToken',val);
      if(val){
        this.navCtrl.setRoot(TabsPage);
      }else{
        this.navCtrl.setRoot(IntroPage);
      }
    })
  } 
}



