import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SearchPage } from '../pages/search/search';
import { MytripsPage } from '../pages/mytrips/mytrips';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { TicketsPage } from '../pages/tickets/tickets';
import { TicketdetailsPage } from '../pages/ticketdetails/ticketdetails';
import { BookingPage } from '../pages/booking/booking';
import { CheckoutPage } from '../pages/checkout/checkout';
import { IntroPage } from '../pages/intro/intro';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { NotificationsPage } from '../pages/notifications/notifications';
import { SettingsPage } from '../pages/settings/settings';
import { MomentModule } from 'angular2-moment';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { FilterPage } from '../pages/filter/filter';
import { ReturnticketsPage } from '../pages/returntickets/returntickets';
import { ReturnticketdetailsPage } from '../pages/returnticketdetails/returnticketdetails';
import { GoticketsPage } from '../pages/gotickets/gotickets';
import { GoticketsdetailsPage } from '../pages/goticketsdetails/goticketsdetails';
import { NativeStorage } from '@ionic-native/native-storage';
import { IonicStorageModule } from '@ionic/storage';
import { RecaptchaModule } from 'ng-recaptcha';
import { ConfirmPage } from '../pages/confirm/confirm';
import { GetticketsProvider } from '../providers/gettickets/gettickets';
import { FormsModule } from '@angular/forms';
import { PaymentmodalPage } from '../pages/paymentmodal/paymentmodal';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TermsPage } from '../pages/terms/terms';
import { BookingProvider } from '../providers/booking/booking';
import { AccountProvider } from '../providers/account/account';
// import {CaptchaModule} from 'primeng/captcha';
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SearchPage,
    MytripsPage,
    MyaccountPage,
    TicketsPage,
    TicketdetailsPage,
    BookingPage,
    CheckoutPage,
    IntroPage,
    RegisterPage,
    LoginPage,
    NotificationsPage,
    SettingsPage,
    FilterPage,
    ReturnticketsPage,
    ReturnticketdetailsPage,
    GoticketsPage,
    GoticketsdetailsPage,
    ConfirmPage,
    PaymentmodalPage,
    TermsPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MomentModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    RecaptchaModule.forRoot(),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SearchPage,
    MytripsPage,
    MyaccountPage,
    TicketsPage,
    TicketdetailsPage,
    BookingPage,
    CheckoutPage,
    IntroPage,
    RegisterPage,
    LoginPage,
    NotificationsPage,
    SettingsPage,
    FilterPage,
    ReturnticketsPage,
    ReturnticketdetailsPage,
    GoticketsPage,
    GoticketsdetailsPage,
    ConfirmPage,
    PaymentmodalPage,
    TermsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    NativeStorage,
    SocialSharing,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    GetticketsProvider,
    BookingProvider,
    AccountProvider
  ]
})
export class AppModule { }
