import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
/*
  Generated class for the BookingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BookingProvider {
  myToken: any;
  myUserInfo: any;
  totalMyGo: any;
  myFullTotal: any;
  totalReturn: any;

  constructor(
    public http: Http,
    public storage: Storage
  ) {
    console.log('Hello BookingProvider Provider');

    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }
    });

    this.storage.get('user_info').then((val) => {
      console.log(val);
      this.myUserInfo = val;
    });

    this.storage.get('myFullTotalPrice').then((val) => {
      console.log(val);
      if (val) {
        this.myFullTotal = val
      }
    })
  }

  reserveTicket(myStringSeats) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', "Bearer" + this.myToken);
    let options = new RequestOptions({
      headers: headers
    });
    return this.http.post('http://dev.tz17.info/public/api/frontEndBookTicket',
      {
        tickets: myStringSeats,
        token: this.myToken
      },
      options
    ).map(res => res.json());
  }

  activatePromoCode(promoCode) {
    return this.http.get('http://dev.tz17.info/public/api/activatePromoCode?token=' + this.myToken + '&totalAmount=' + this.myFullTotal + '&promocode=' + promoCode).map(res => res.json())
  }

  chargeFawry(id, total, products) {
    return this.http.get('http://payments2.bit68.com/fawry?user_id=' + this.myToken + '&order_id=' + id + '&amount=' + total + '&mobile_number=' + this.myUserInfo.phone + '&email=' + this.myUserInfo.email + '&items=' + JSON.stringify(products)).map(res => res.json());
  }

}
