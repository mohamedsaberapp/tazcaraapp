import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
/*
  Generated class for the AccountProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AccountProvider {

  constructor(public http: Http) {
    console.log('Hello AccountProvider Provider');
  }

  login(e, p) {
    return this.http.post('http://dev.tz17.info/public/api/agentLogin',
      {
        email: e,
        password: p
      }
    ).map(res => res.json());
  }

  register(n, p, dob, e, password, password_c, g) {
    return this.http.post('http://dev.tz17.info/public/api/postAgentRegister',
      {
        name: n,
        phone: p,
        dob: dob,
        email: e,
        password: password,
        password_confirmation: password_c,
        gender: g
      }
    ).map(res => res.json())
  }


  resetPassword(e) {
    return this.http.post('http://dev.tz17.info/public/api/resetPassword',
      {
        email: e
      }
    ).map(res => res.json());
  }

  getProfile(token) {
    return this.http.get('http://dev.tz17.info/public/api/getProfile?token=' + token).map(res => res.json());
  }


  verifyAccount(sms, email) {
    return this.http.post('http://dev.tz17.info/public/api/postAgentConfirmation',
      {
        email: email,
        code: sms
      }
    ).map(res => res.json());
  }
}
