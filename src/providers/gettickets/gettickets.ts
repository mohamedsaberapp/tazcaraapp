import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


/*
  Generated class for the GetticketsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GetticketsProvider {

  apiUrl: string;
  myTrips: any;

  constructor(public http: Http) {
    console.log('Hello GetticketsProvider Provider');
    this.apiUrl = 'http://dev.tz17.info/public/api/'
  }


  getTickets(goDate, from, to) {
    return this.http.get(this.apiUrl + from + "/" + to + "/" + goDate + "/All/1/1/").map(res => res.json());
  }

  getTicketsRound(goDate, returnDate, from, to) {
    return this.http.get(this.apiUrl + from + "/" + to + "/" + goDate + "/All/1/2/" + returnDate).map(res => res.json());
  }

  getSearchData(){
    return this.http.get(this.apiUrl+'getSearchData').map(res=>res.json());
  }
}
