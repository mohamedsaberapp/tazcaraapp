import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { PaymentmodalPage } from '../paymentmodal/paymentmodal';
import { TabsPage } from '../tabs/tabs';
import { BookingProvider } from '../../providers/booking/booking';


/**
 * Generated class for the BookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage {
  myFullTotal: number;
  totalReturn: number;
  totalMyGo: number;
  ticket_price: any;
  myStringSeats: any;
  totalPromo: any;
  promoCode: string;
  total: any;
  reserveData: any;
  myOrderId: any;
  myUserInfo: any;
  booking_date: any;
  no_of_tickets: any;
  myPayment: any;
  myToken: any;
  products: any;
  myGo: any;
  myReturnTrip: any;
  myGoTrip: any = {
    companyDetails: [
      { official_name: '' }
    ]
  };
  way: string = "go";
  promoAdded: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    private toast: ToastController,  
    private storage: Storage, 
    private loadingCtrl: LoadingController, 
    public modalCtrl: ModalController,
    public bookingService:BookingProvider
  ) {

    this.totalReturn = this.navParams.get('totalReturn');
    console.log(this.totalReturn);

    this.total = this.navParams.get('total');
    this.myStringSeats = this.navParams.get('myStringSeats');
    this.booking_date = this.navParams.get('date');
    this.products = this.navParams.get('products');
    this.no_of_tickets = this.navParams.get('no_of_tickets');
    this.ticket_price = this.navParams.get('ticket_price');

    this.storage.get('totalmyGo').then((val) => {
      console.log(val);
      if (val) {
        this.totalMyGo = val;
        this.myFullTotal = this.totalMyGo + this.totalReturn;
        this.storage.set('myFullTotalPrice',this.myFullTotal);
      }
    })

    console.log('myFullTotal', this.myFullTotal);
    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }
    });
    this.storage.get('user_info').then((val) => {
      console.log(val);
      this.myUserInfo = val;
    });
    this.storage.get('myGoFullTrip').then((val) => {
      console.log(val);
      if (val) {
        this.myGoTrip = val;
      }
    })
    this.storage.get('myGo').then((val) => {
      console.log(val);
      if (val) {
        this.myGo = val;
      }
    })
    this.storage.get('myReturnFullTrip').then((val) => {
      console.log(val);
      if (val) {
        this.myReturnTrip = val;
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingPage');
  }

  //reserve ticket 
  reserve() {
    let loading = this.loadingCtrl.create({
      content: '<img src="assets/imgs/preloader.gif"/>',
      spinner: 'hide',
      cssClass: 'transparent'
    });
    loading.present();
    //call booking service to reserve ticket
    this.bookingService.reserveTicket(this.myStringSeats).subscribe((data: any) => {
      console.log(data);
      if (data.merchant_reference) {
        this.reserveData = data;
        this.myOrderId = data.merchant_reference;
        loading.dismiss();
        this.chargeFawry(this.myOrderId, this.reserveData.amount);
      }
      else {
        loading.dismiss();
        let toast = this.toast.create({
          message: data.response,
          position: 'top',
          duration: 2000
        })
        toast.present();
      }
    }, err => {
      console.log(err);
      loading.dismiss();
    })
  }

  openModalPayment() {
    let modal = this.modalCtrl.create(PaymentmodalPage,{},{cssClass:'paymentModal'});
    modal.onDidDismiss(data => {
      console.log(data);
      
      this.navCtrl.setRoot(TabsPage);
    });
    modal.present();
  }

  //call fawry api
  chargeFawry(id, total) {
    let loading = this.loadingCtrl.create({
      content: '<img src="assets/imgs/preloader.gif"/>',
      spinner: 'hide',
      cssClass: 'transparent'
    });
    loading.present();
    console.log(this.products);
    this.bookingService.chargeFawry(
      id,total,this.products).subscribe((data: any) => {
      console.log(data);
      if (data.referenceNumber) {
        loading.dismiss();
        let toast = this.toast.create({
          message: data.statusDescription,
          position: 'top',
          duration: 2000
        })
        toast.present().then(() => {
          this.goCheckout();
        })
      } else {
        loading.dismiss();
        let toast = this.toast.create({
          message: data.statusDescription,
          position: 'top',
          duration: 2000
        })
        toast.present();
      }
    }, err => {
      let toast = this.toast.create({
        message: err.statusText,
        position: 'top',
        duration: 2000
      })
      toast.present();
      console.log(err.statusText);
      loading.dismiss();
    })
  }

  //activate promo code
  activate() {
    console.log(this.promoCode);
    this.bookingService.activatePromoCode(this.promoCode).subscribe((data: any) => {
      console.log(data);
      if (data.response.totalAmount) {
        this.promoAdded = true;
        this.myFullTotal = data.response.totalAmount;
        console.log('added');
      } else {
        let toast = this.toast.create({
          message: data.response,
          position: 'top',
          duration: 2000
        })
        toast.present()
      }
    }, err => {
      console.log(err);
    })
  }

  //just a modal to show success booking
  goCheckout() {
    this.storage.remove('myReturnFullTrip').then(() => {
      this.openModalPayment();
    })
  }
}
