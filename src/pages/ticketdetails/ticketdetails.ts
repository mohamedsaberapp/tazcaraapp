import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Platform } from 'ionic-angular';
import { BookingPage } from '../booking/booking';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { LoginPage } from '../login/login';

/**
 * Generated class for the TicketdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ticketdetails',
  templateUrl: 'ticketdetails.html',
})
export class TicketdetailsPage {

  prevView: any;
  reserveData: any;
  myOrderId: any;
  myUserInfo: any;
  product: any;
  products: any;
  myMaxTickets: any;
  myToken: any;
  myStringSeats: any;
  myNewBus: any;
  chart: any;
  mySeats: any[];
  trip: any;
  myBus: ({ number: number; color: string; } | { number: string; color: string; })[];
  backseats: any[];
  total: number = 0;
  tickets: number = 5;
  showPrice: boolean = false;
  bus: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private http: Http, private storage: Storage, private loadingCtrl: LoadingController, public platform: Platform) {
    // platform.registerBackButtonAction(() => {
    //   this.navCtrl.push(Tick)
    // });

    this.products = [];
    this.product = {};
    this.trip = this.navParams.get('trip');
    this.myNewBus = [];
    this.chart = [];
    this.mySeats = [];
    this.chart = this.trip.chartArray;
    this.storage.get('max_tickets').then((val) => {
      console.log(val);
      this.myMaxTickets = val;
    });
    this.storage.get('user_info').then((val) => {
      console.log(val);
      this.myUserInfo = val;
    });
    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }

    });
    for (let x = 0; x < this.trip.chartArray.length; x++) {
      if (this.chart[x].class !== 'End of Row') {
        this.chart[x]['id'] = this.trip.id;
        this.chart[x]['city_to'] = this.trip.city_to;
        this.chart[x]['tripTravelDate'] = this.trip.tripTravelDate;
        this.chart[x]['trip_id'] = this.trip.trip_id;
        //repeat assigning keys for fawry endpoint
        this.chart[x]['boardingPoint'] = this.trip.id;
        this.chart[x]['travelTo'] = this.trip.city_to;
        this.chart[x]['travelDate'] = this.trip.tripTravelDate;
        this.chart[x]['seatNumber'] = this.chart[x]['seatNo'];
        this.myNewBus.push(this.chart[x]);

      }
    }
    console.log(this.myNewBus);
    console.log(this.mySeats);
  }
  ionViewWillLeave() {
    console.log('leaving tickets details');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TicketdetailsPage');

  }
  ionViewDidEnter() {
    this.mySeats = [];
    this.products = [];
    this.product = {};
    this.total = 0;
    this.showPrice = false;
    this.prevView = this.navCtrl.last().component.name;
    console.log(this.prevView);
    if (this.prevView == 'BookingPage' && !this.total) {
      for (let x = 0; x < this.myNewBus.length; x++) {
        if (this.myNewBus[x].color == 'green') {
          this.myNewBus[x].color = 'primary';
        }
      }
    } else if (this.prevView == 'TicketsPage' && !this.total) {
      for (let x = 0; x < this.myNewBus.length; x++) {
        if (this.myNewBus[x].color == 'green') {
          this.myNewBus[x].color = 'gold';
        }
      }
    }


    console.log('entered');
  }
  book() {
    this.storage.remove('myReturnFullTrip');
    if (this.myToken) {
      this.myStringSeats = JSON.stringify(this.mySeats);
      console.log(this.myStringSeats);
      let loading = this.loadingCtrl.create({
        content: '<img src="assets/imgs/preloader.gif"/>',
        spinner: 'hide',
        cssClass: 'transparent'
      })
      loading.present();
      this.http.get('http://dev.tz17.info/public/api/check/' + this.myStringSeats).map(res => res.json()).subscribe((data: any) => {
        console.log(data);
        if (data.availability === 1) {
          console.log('available');
          loading.dismiss();
          this.chargeFawry();
        } else {
          console.log('not available');
          loading.dismiss();
        }
      }, err => {
        console.log(err);
        loading.dismiss();
      })
    } else {
      this.navCtrl.push(LoginPage);
    }
  }
  choose(s) {
    if (this.prevView == 'BookingPage' && !this.total) {
      if (s.color == 'green') {
        // let bookedtoast = this.toast.create({
        //   message: 'لقد حجزت هذا الكرسي بالفعل',
        //   position: 'top',
        //   duration: 2000
        // })
        // bookedtoast.present();
        s.color = 'primary';
      }
    } else if (this.prevView == 'TicketsPage' && !this.total) {
      if (s.color == 'green') {
        s.color = 'gold';
      }
    }
    if (s.color == 'primary') {
      let bookedtoast = this.toast.create({
        message: 'هذا الكرسي محجوز بالفعل',
        position: 'top',
        duration: 2000
      })
      bookedtoast.present();
    } else if (s.color == 'green') {
      s.color = 'gold';
      this.mySeats.splice(this.mySeats.indexOf(s.number), 1);
      if (this.total != 0) {
        this.total -= this.trip.ticket_price;
      }
    } else if (s.color == 'light') {
      s.color = 'light';
    } else if (s.color == 'grey') {
      s.color = 'grey';
    } else {
      if (this.mySeats.length >= this.myMaxTickets) {
        let toast = this.toast.create({
          message: 'لا يمكنك حجز اكثر من ذلك',
          position: 'top',
          duration: 2000
        })
        toast.present();
      } else {
        s.color = 'green';
        this.mySeats.push(s);
        console.log(this.mySeats);
        this.total += this.trip.ticket_price;
      }
      this.showPrice = true;
      console.log(this.total);
    }
  }
  // reserve() {
  //   let headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   headers.append('Authorization', "Bearer" + this.myToken);
  //   let options = new RequestOptions({
  //     headers: headers
  //   });
  //   let loading = this.loadingCtrl.create({
  //     content: ''
  //   });
  //   loading.present();
  //   this.http.post('http://dev.tz17.info/public/api/frontEndBookTicket',
  //     {
  //       tickets: this.myStringSeats,
  //       token: this.myToken
  //     },
  //     options
  //   ).map(res => res.json()).subscribe((data: any) => {
  //     console.log(data);
  //     if (data.merchant_reference) {
  //       this.reserveData = data;
  //       this.myOrderId = data.merchant_reference;
  //       loading.dismiss();
  //       this.chargeFawry(this.myOrderId, this.reserveData);
  //     }
  //     else {
  //       loading.dismiss();
  //       let toast = this.toast.create({
  //         message: data.response,
  //         position: 'top',
  //         duration: 2000
  //       })
  //       toast.present();
  //     }
  //   }, err => {
  //     console.log(err);
  //     loading.dismiss();
  //   })
  // }
  chargeFawry() {
    var today = new Date();
    var dd: any = today.getDate();
    var mm: any = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    var myCurrentDate = yyyy + '-' + mm + '-' + dd;
    this.product.price = this.trip.ticket_price;
    this.product.quantity = this.mySeats.length;
    this.products.push(this.product);
    console.log(this.products);
    this.navCtrl.push(BookingPage, {
      // data: reserveData,
      date: myCurrentDate,
      products: this.products,
      // myOrderId:id,
      myStringSeats: this.myStringSeats,
      total: this.total,
      no_of_tickets: this.mySeats.length,
      ticket_price: this.trip.ticket_price
    });
  }


}
