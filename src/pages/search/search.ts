import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ModalController, ToastController, LoadingController, Refresher } from 'ionic-angular';
import { TicketsPage } from '../tickets/tickets';
import { NotificationsPage } from '../notifications/notifications';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GoticketsPage } from '../gotickets/gotickets';
import { Storage } from '@ionic/storage';
import { GetticketsProvider } from '../../providers/gettickets/gettickets';
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  toDestinationTemp: any[];
  goDateSearch: any;
  tempDate: string;
  formatedDate: any;
  modifiedSecond: string;
  modifiedMonth: string;
  plusDay: any;
  goDateMax: any;
  goMax: string;
  returnDateMax: any;
  maxSelectabledate: any;
  minDate: any;
  mySearch: any;
  myLastSearches: any[];
  topRoutes: any;
  myToken: any;
  myFilteredTos: any[];
  bookList: any;
  myTrips: any;
  myTickets: any;
  tripNumber: number;
  myTos: any;
  destinations: any[];
  tabBarElement: any;
  trip: boolean;
  round: string;
  go: string;
  fromDestination: any;
  toDestination: any;
  goDate: any;
  returnDate: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private app: App,
    public modalCtrl: ModalController,
    private http: Http,
    private toastCtrl: ToastController,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private ticketService:GetticketsProvider
  ) {
    //returnDate
    this.returnDate = new Date();
    console.log(this.returnDate);
    this.returnDate.setDate(this.returnDate.getDate() + 1);
    this.returnDate = this.returnDate.toISOString();
    console.log(this.returnDate);
    //returnDate Max
    this.returnDateMax = new Date();
    this.returnDateMax.setDate(this.returnDateMax.getDate() + 15);
    this.returnDateMax = this.returnDateMax.toISOString();

    //goDate
    this.goDate = new Date().toISOString();
    //goDateMax
    this.goDateMax = new Date();
    this.goDateMax.setDate(this.goDateMax.getDate() + 15);
    this.goDateMax = this.goDateMax.toISOString();

    //other declarations
    this.fromDestination = 1;
    this.trip = true;
    this.tripNumber = 1;
    this.round = "grey";
    this.go = "blue";
    this.myLastSearches = [];
    //get token
    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }
    });
    //get all storage elements
    this.storage.forEach((value, key, index) => {
      console.log("This is the value", value)
      console.log("from the key", key)
      console.log("Index is", index)
    });
    let loading = this.loadingCtrl.create({
      content: '<img src="assets/imgs/preloader.gif"/>',
      spinner: 'hide',
      cssClass: 'transparent'
    });
    loading.present();
    this.http.get("http://dev.tz17.info/public/api/getSearchData").map(res => res.json()).subscribe(data => {
      console.log(data);
      this.destinations = data.destinations;
      this.fromDestination = 1;
      this.initCairoAlex(1);
      loading.dismiss();
    },
      err => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Error getting destinations: ' + err,
          duration: 3000
        })
        toast.present();
      });
  }
  //decimal function to convert all numbers <10 into 2 digits i.e '01'
  decimal(n: number) {
    if (n < 10) {
      return '0' + n;
    } else {
      return n;
    }
  }
  //onChange of date picker
  dateChange(value) {
    console.log(value);
    this.tempDate = value.year + '-' + this.decimal(value.month) + '-' + this.decimal(value.day) + 'T' + this.decimal(value.hour) + ':' + this.decimal(value.minute) + ':' + this.decimal(value.second) + '.' + this.decimal(value.millisecond) + 'Z';
    console.log('tempdate', JSON.stringify(this.tempDate));

    this.formatedDate = new Date(this.tempDate);
    console.log('formatedDate', this.formatedDate);
    this.formatedDate.setDate(this.formatedDate.getDate() + 1);
    this.formatedDate = this.formatedDate.toISOString();
    console.log(this.formatedDate);
    this.returnDate = this.formatedDate;
  }
  ionViewDidEnter() {
    console.log('entered');
    //get top routes
    this.http.get('http://dev.tz17.info/public/api/top-routes').map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      this.topRoutes = data;
    }, err => {
      console.log(err);
    })

  }

  chooseToDestination(e) {
    console.log(e);
    // console.log(this.destinations);
    let destinationId = e;

    this.myTos = this.destinations.filter(destination =>
      destination.id == destinationId
    );
    console.log(this.myTos);
    this.myFilteredTos = this.myTos[0].listOfTos;

  }

  initCairoAlex(x) {
    console.log(x);
    // console.log(this.destinations);
    let destinationId = x;

    this.myTos = this.destinations.filter(destination =>
      destination.id == destinationId
    );
    console.log(this.myTos);
    this.myFilteredTos = this.myTos[0].listOfTos;
    // this.toDestination = this.myFilteredTos[0].id;

    this.toDestinationTemp = this.myFilteredTos.filter(myFilteredTo =>
      myFilteredTo.id == 2
    );
    this.toDestination = this.toDestinationTemp[0].id;
    console.log(this.toDestination);
  }
  ///{from}/{to}/{date}/{company}/{number_of_seats}/{round}/{backdate}
  goSearch() {
    //reset date calendars
    setTimeout(() => {
      //goDate
      this.goDate = new Date().toISOString();
      //goDateMax
      this.goDateMax = new Date();
      this.goDateMax.setDate(this.goDateMax.getDate() + 15);
      this.goDateMax = this.goDateMax.toISOString();
      //returnDate
      this.returnDate = new Date();
      console.log(this.returnDate);
      this.returnDate.setDate(this.returnDate.getDate() + 1);
      this.returnDate = this.returnDate.toISOString();
      console.log(this.returnDate);
      //returnDate Max
      this.returnDateMax = new Date();
      this.returnDateMax.setDate(this.returnDateMax.getDate() + 15);
      this.returnDateMax = this.returnDateMax.toISOString();
    }, 3000);
    //in case he choosed round trip 
    if (this.tripNumber == 2) {
      //if he didn't pick a date
      if (!this.returnDate) {
        let toast = this.toastCtrl.create({
          message: ' من فضلك اختار تاريخ العودة',
          position: 'top',
          duration: 2000
        })
        toast.present();
      } else {
        //able to search
        let loading = this.loadingCtrl.create({
          content: '<img src="assets/imgs/preloader.gif"/>',
          spinner: 'hide',
          cssClass: 'transparent'
        });
        loading.present();
        //call round trip service
        this.ticketService.getTicketsRound(
          this.goDate.split('T')[0],
          this.returnDate.split('T')[0],
          this.fromDestination,
          this.toDestination
        ).subscribe((data: any) => {
          loading.dismiss();
          console.log('Round Trip Data => ', data);
          this.storage.set('max_tickets', data.Trips.max_tickets_per_request).then(() => {
            console.log(this.storage.get('max_tickets'));
          })
          if(data){
            this.myTrips = data;
          }
          if (this.myTrips.Trips.returnTrips) {
            this.storage.set('myReturnDetails', this.myTrips.Trips.returnTrips).then((val) => {
              console.log(val);
            })
          }
          //in case there're no trips in the selected date
          if (!this.myTrips.Trips.trips[0]) {
            let toast = this.toastCtrl.create({
              message: 'لا توجد رحلات في هذا اليوم',
              position: 'top',
              duration: 2000
            })
            toast.present();
          }
          else {
            //if there're trips then navigate to the next view
            this.app.getRootNav().push(GoticketsPage, {
              trips: this.myTrips,
              start_date: this.goDate.split('T')[0],
              start_date_formatted: this.goDate,
              return_date: this.returnDate.split('T')[0],
              return_date_formatted: this.returnDate,
              from: this.fromDestination,
              to: this.toDestination
            });
          }
        },
          err => {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: err,
              duration: 3000
            });
            toast.present();
          });
      }
    } else {
      //in case he choosed single trip
      let loading = this.loadingCtrl.create({
        content: '<img src="assets/imgs/preloader.gif"/>',
        spinner: 'hide',
        cssClass: 'transparent'
      });
      loading.present();
      //call single trip service
      this.ticketService.getTickets(
        this.goDate.split('T')[0],
        this.fromDestination,
        this.toDestination
      ).subscribe((data: any) => {
        loading.dismiss();
        console.log(data);
        this.storage.set('max_tickets', data.Trips.max_tickets_per_request).then(() => {
          console.log(this.storage.get('max_tickets'));
        })
        if(data){
          this.myTrips = data;
        }
        //in case there're no trips in the selected date
        if (!this.myTrips.Trips.trips[0]) {
          let toast = this.toastCtrl.create({
            message: 'لا توجد رحلات في هذا اليوم',
            position: 'top',
            duration: 2000
          })
          toast.present();
        }
        else {
          //if there're trips then navigate to the next view
          this.app.getRootNav().push(TicketsPage, {
            trips: this.myTrips,
            start_date: this.goDate.split('T')[0],
            start_date_formatted: this.goDate,
            from: this.fromDestination,
            to: this.toDestination
          });
        }
      },
        err => {
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: err,
            duration: 3000
          });
          toast.present();
        });
    }
  }

  onChange(e) {
    if (e == true) {
      this.tripNumber = 1;
      this.go = "blue";
      this.round = "grey";
    } else {
      this.tripNumber = 2;
      this.round = "secondary";
      this.go = "grey";
    }
  }

  openNotifications() {
    let modal = this.modalCtrl.create(NotificationsPage);
    modal.present();
  }

  doRefresh(refresher) {
    //goDate
    this.goDate = new Date().toISOString();

    //goDateMax
    this.goDateMax = new Date();
    this.goDateMax.setDate(this.goDateMax.getDate() + 15);
    this.goDateMax = this.goDateMax.toISOString();

    //returnDate
    this.returnDate = new Date();
    this.returnDate.setDate(this.returnDate.getDate() + 1);
    this.returnDate = this.returnDate.toISOString();

    //returnDate Max
    this.returnDateMax = new Date();
    this.returnDateMax.setDate(this.returnDateMax.getDate() + 15);
    this.returnDateMax = this.returnDateMax.toISOString();

    //get token
    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }
    });

    //show all storage elements (debugging use only)
    this.storage.forEach((value, key, index) => {
      console.log("This is the value", value)
      console.log("from the key", key)
      console.log("Index is", index)
    });

    //get all search data from the service ( all cities )
    this.ticketService.getSearchData().subscribe(data => {
      console.log(data);
      refresher.complete();
      if(data.destinations){
        this.destinations = data.destinations;
      }
    },
      err => {
        refresher.complete();
        let toast = this.toastCtrl.create({
          message: 'Error getting destinations: ' + err,
          duration: 3000
        })
        toast.present();
      });

  }
}


