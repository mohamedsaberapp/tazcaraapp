import { Component } from '@angular/core';

import { SearchPage } from '../search/search';
import { MytripsPage } from '../mytrips/mytrips';
import { MyaccountPage } from '../myaccount/myaccount';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = SearchPage;
  tab2Root = MytripsPage;
  tab3Root = MyaccountPage;

  constructor() {

  }
}
