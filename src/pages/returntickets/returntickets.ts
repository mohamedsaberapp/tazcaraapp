import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ViewController } from 'ionic-angular';
import { ReturnticketdetailsPage } from '../returnticketdetails/returnticketdetails';
import { FilterPage } from '../filter/filter';
import { Storage } from '@ionic/storage';
import { GetticketsProvider } from '../../providers/gettickets/gettickets';

/**
 * Generated class for the ReturnticketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-returntickets',
  templateUrl: 'returntickets.html',
})
export class ReturnticketsPage {
  myCompany: any;
  initTrips: any[];
  formatReturnDate: Date;
  returnStartDate: string;
  myChoosedDate: any;
  returnDate: any;
  myPrevDay: any;
  myNextDay: any;
  prevView: any;
  city_to: any;
  city_from: any;
  from: any;
  to: any;
  goDate: any;
  myTrips: any[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private storage: Storage,
    public getticketsProvider: GetticketsProvider,
    private loading: LoadingController
  ) {
    this.myTrips = [];
    this.storage.get('myGoFullTrip').then((val) => {
      console.log(val);
      if (val) {
        this.myCompany = val.companyDetails[0].id;
        console.log(this.myCompany);
      }
    })
    this.storage.get('myReturnDetails').then((val) => {
      console.log(val);
      if (val) {
        this.myTrips = val.filter(myTrip => {
          return myTrip.companyDetails[0].id == this.myCompany;
        })
        this.initTrips = this.myTrips;
        this.city_from = this.myTrips[0].city_from;
        this.city_to = this.myTrips[0].city_to;
        this.storage.set('city_ids', this.myTrips[0].city_from + '&' + this.myTrips[0].city_to);
      }
    })
    this.goDate = this.navParams.get('start_date');
    this.storage.get('city_return_date').then((val) => {
      console.log('city_return_date', val);
      this.returnDate = val;
      this.formatReturnDate = new Date(val);
    })
    this.storage.get('choosedGoDate').then((val) => {
      console.log('choosed Date', val);
      this.myChoosedDate = val;
      let tempDate = new Date(this.myChoosedDate);
      if (tempDate > this.formatReturnDate) {
        tempDate.setDate(tempDate.getDate() + 1);
        console.log(tempDate);
        this.returnStartDate = tempDate.toISOString().split('T')[0];
      } else {
        this.returnStartDate = this.returnDate;
      }
    })
    console.log(this.myTrips);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReturnticketsPage');
  }

  ionViewDidEnter() {
    //check the previous view and handle if the user clicked on the back button
    let previousView: ViewController = this.navCtrl.last();
    console.log(previousView.component.name);
    if (previousView.component.name == 'ReturnticketdetailsPage') {
      this.storage.get('city_ids').then((val) => {
        console.log(val);
        if (val) {
          let loading = this.loading.create({
            content: '<img src="assets/imgs/preloader.gif"/>',
            spinner: 'hide',
            cssClass: 'transparent'
          });
          loading.present();
          this.city_from = val.split('&')[1];
          this.city_to = val.split('&')[0];
          this.getticketsProvider.getTicketsRound(
            this.myChoosedDate, 
            this.returnStartDate, 
            this.city_from, 
            this.city_to
          ).subscribe((data: any) => {
            console.log(data);
            this.myTrips = data.Trips.returnTrips.filter(myTrip => {
              return myTrip.companyDetails[0].id == this.myCompany;
            })
            loading.dismiss();
          })
        } else {
          console.log('no saved ids cuz of empty value for city_ids');
        }
      })
    } else {
      console.log('no refresh needed cuz the previous view is ', previousView.component.name);
    }
  }

  //show tickets of the next day
  goNextDay() {

    let nextDate = new Date(this.returnStartDate);
    nextDate.setDate(nextDate.getDate() + 1);
    console.log(nextDate);
    this.myNextDay = nextDate.toISOString().split('T')[0];
    this.returnStartDate = this.myNextDay;
    console.log('final date', this.myNextDay);
    let loading = this.loading.create({ content: '' });
    loading.present();
    this.getticketsProvider.getTicketsRound(this.myChoosedDate, this.returnStartDate, this.city_from, this.city_to).subscribe((data: any) => {
      console.log(data);
      this.myTrips = data.Trips.returnTrips.filter(myTrip => {
        return myTrip.companyDetails[0].id == this.myCompany;
      })
      loading.dismiss();
    })
  }

  //show tickets of the previous day
  goPrevDay() {
    let prevDate = new Date(this.returnStartDate);
    prevDate.setDate(prevDate.getDate() - 1);
    console.log(prevDate);
    this.myPrevDay = prevDate.toISOString().split('T')[0];
    this.returnStartDate = this.myPrevDay;
    console.log('final date', this.myPrevDay);
    let loading = this.loading.create({ content: '' });
    loading.present();
    this.getticketsProvider.getTicketsRound(
      this.myChoosedDate,
      this.returnStartDate, 
      this.city_from, 
      this.city_to
    ).subscribe((data: any) => {
      console.log(data);
      this.myTrips = data.Trips.returnTrips.filter(myTrip => {
        return myTrip.companyDetails[0].id == this.myCompany;
      })
      loading.dismiss();
    })
  }

  goDetails(trip) {
    this.navCtrl.push(ReturnticketdetailsPage, { trip: trip });
    //store the information and data of the trip to use in the upcoming views
    this.storage.set('myReturnFullTrip', trip).then(() => {
      this.storage.get('myReturnFullTrip').then((val) => {
        console.log(val);
      })
    })
  }

  //filter tickets
  filter() {
    let modal = this.modalCtrl.create(FilterPage, { data: this.initTrips });
    modal.onDidDismiss(data => {
      if (data) {
        this.myTrips = data;
      }
      console.log('Filter Data :', data);
    })
    modal.present();
  }

}
