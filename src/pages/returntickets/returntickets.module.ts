import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReturnticketsPage } from './returntickets';

@NgModule({
  declarations: [
    ReturnticketsPage,
  ],
  imports: [
    IonicPageModule.forChild(ReturnticketsPage),
  ],
})
export class ReturnticketsPageModule {}
