import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  myFilterOptions: { 'min_price': number; 'max_price': number; 'boarding_points': any[]; 'classes': any[]; 'bus_sizes': any[]; 'companies': any[]; };
  unbooked: boolean = true;
  myFilteredBooks: any = [];
  size: any = [];
  company: any = [];
  class: any = [];
  myFilteredClasses: any = [];
  boarding: any = [];
  myFilteredBoardings: any = [];
  myFilteredPrices: any = [];
  facility: any;
  data: any = [];
  dbtimeslots: any[];
  myFilteredTrips: any = [];
  myTrips: any = [];
  price: { upper: number, lower: number } = {
    upper: 250,
    lower: 50
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private loadingCtrl: LoadingController) {
    this.data = [];
    this.myTrips = this.navParams.get('data');
    console.log(this.myTrips);
    this.getFilterOptions();
    this.price.upper = this.myFilterOptions.max_price;
    this.price.lower = this.myFilterOptions.min_price;
  }

  getFilterOptions() {
    let loading = this.loadingCtrl.create({
      content: '<img src="assets/imgs/preloader.gif"/>',
      spinner: 'hide',
      cssClass: 'transparent'
    })
    loading.present();
    let data = this.myTrips;
    let min_price = 10000;
    let max_price = 0;
    let boarding_points = [];
    let classes = [];
    let bus_sizes = [];
    let companies = [];
    for (var i = 0; i < data.length; i++) {
      console.log(i);
      if (companies.indexOf(data[i].companyDetails[0].name) < 0) {
        companies.push(data[i].companyDetails[0].name);
      }
      if (classes.indexOf(data[i].trip_class) < 0) {
        classes.push(data[i].trip_class);
      }
      if (bus_sizes.indexOf(data[i].number_of_seats) < 0) {
        bus_sizes.push(data[i].number_of_seats);
      }
      if (boarding_points.indexOf(data[i].boarding_point) < 0) {
        boarding_points.push(data[i].boarding_point);
      }
      if (data[i].ticket_price > max_price) {
        max_price = data[i].ticket_price
      }
      if (data[i].ticket_price < min_price) {
        min_price = data[i].ticket_price
      }
    }
    console.log('min_price', min_price,
      'max_price', max_price,
      'boarding_points', boarding_points,
      'classes', classes,
      'bus_sizes', bus_sizes,
      'companies', companies);
    loading.dismiss();
    return this.myFilterOptions = {
      'min_price': min_price,
      'max_price': max_price,
      'boarding_points': boarding_points,
      'classes': classes,
      'bus_sizes': bus_sizes,
      'companies': companies
    }

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  filter() {
    // console.log(boarding);
    console.log('boarding points: ', this.boarding);
    console.log('unbooked: ', this.unbooked);

    //filtering with price range
    this.myFilteredPrices = this.myTrips.filter((myTrip) => {
      return myTrip.ticket_price >= this.price.lower &&
        myTrip.ticket_price <= this.price.upper;
    }
    );
    console.log('My filtered items based on Price Range: ', this.myFilteredPrices);

    //filtering with boarding points
    this.myFilteredBoardings = this.myFilteredPrices.filter((myFilteredPrice) => {
      if (this.boarding.length > 0) {
        for (let x = 0; x < this.boarding.length; x++) {
          return myFilteredPrice.boarding_point == this.boarding[x]
        }
      } else {
        return this.myFilteredPrices;
      }
    });
    console.log('My filtered items based on Boarding Points: ', this.myFilteredBoardings);


    //filtering with ticket class
    this.myFilteredClasses = this.myFilteredBoardings.filter((myFilteredBoarding) => {
      if (this.class.length > 0) {
        for (let z = 0; z < this.class.length; z++) {
          return myFilteredBoarding.trip_class == this.class[z]
        }
      } else {
        return this.myFilteredBoardings;
      }
    });
    console.log('My filtered items based on Trip Class: ', this.myFilteredClasses);



    //test filter unbooked trips only
    // this.myFilteredBooks = this.myFilteredClasses.filter((myFilteredClass) => {
    //   return myFilteredClass.bookedTickets.length == 0
    // });
    // console.log(this.myFilteredBooks);

    let data = this.myFilteredClasses;
    console.log('My Final Filtered Data: ', data);
    this.viewCtrl.dismiss(data);

  }


}
