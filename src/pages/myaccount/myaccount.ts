import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { SettingsPage } from '../settings/settings';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';

/**
 * Generated class for the MyaccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html',
})
export class MyaccountPage {

  myToken: any;
  user_info: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage:Storage,private app:App) {
    this.storage.get('user_info').then((val)=>{
      console.log('user_info',val);
      if(val){
        this.user_info=val;
      }
      
    });
    this.storage.get('myToken').then((val)=>{
      console.log('token',val);
      if(val){
        this.myToken=val;
      }
      
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyaccountPage');
  }
  goAbout() {
    this.navCtrl.push(AboutPage);
  }
  goSettings() {
    this.navCtrl.push(SettingsPage);
  }
  goLog(){
    this.app.getRootNav().setRoot(LoginPage);
  }
  logOut(){
    this.storage.clear().then(()=>{
      this.app.getRootNav().setRoot(LoginPage);
    })
  }
}
