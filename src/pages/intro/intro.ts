import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {
slides:any[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.slides = [
      {
        title: "About TAZCARA",
        description: "TAZCARA combine all bus operators to let you book your trip online in Egypt.",
        description2: "منصة تذكرة بتجمع كل شركات النقل البري عشان تقدر تقارن وتحجز رحلاتك من مكان واحد",
        image: "assets/imgs/bus.png",
        image2:"assets/imgs/micro_bus.png"
      },
      {
        title: "Book your trip easier",
        description: "Select trip destination,date,seats and TAZCARA will search for you.",
        description2: "حدد عايز تسافر فين وامتي واضغط بحث .. و تذكرة حتدورلك في كل اتوبيسات النقل البري",
        image: "assets/imgs/slide1.png",
      },
      {
        title: "Choose you seat",
        description: "Select your bus seats and boarding poing.",
        description2: "حنجبلك كل الاتوبيسات .. وانت قارن بالسعر و الخدمة و محطة الركوب الاقرب ليك",
        image: "assets/imgs/slide2.png",
      }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
  }
  start(){
    this.navCtrl.setRoot(TabsPage);
  }

}
