import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { RegisterPage } from '../register/register';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  myLang: string = "اللغه";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private actionSheetCtrl: ActionSheetController,
    private storage:Storage,
    public app:App
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  showLanguage() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Pick your language',
      buttons: [
        {
          text: 'عربي',
          handler: () => {
            console.log('Destructive clicked');
            this.myLang = 'عربي';
          }
        }, {
          text: 'English',
          handler: () => {
            console.log('Archive clicked');
            this.myLang = 'English';
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  logOut() {
    this.storage.clear().then((val)=>{
      console.log(this.storage);
      this.app.getRootNav().setRoot(RegisterPage);
    })
    
  }
}
