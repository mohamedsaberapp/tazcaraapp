import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TermsPage } from '../terms/terms';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, private socialSharing: SocialSharing) {

  }

  socialShare() {
    this.socialSharing.share("TAZCARA", "Get The App Now", "", "https://bit68.com").
      then(() => {
        console.log("Sharing success");
        // Success!
      }).catch(() => {
        // Error!
        console.log("Share failed");
      });
  }
  goTerms(){
    this.navCtrl.push(TermsPage);
  }

}
