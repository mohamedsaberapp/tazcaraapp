import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { IntroPage } from '../intro/intro';
import 'rxjs/add/operator/map';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';
import { AccountProvider } from '../../providers/account/account';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
/* 6LfjnzQUAAAAABLbPAtV52_wauIGhAGuR7UEtGVd RECAPTCHA KEY*/


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private toastCtrl: ToastController,  
    public zone: NgZone, 
    public storage: Storage, 
    public loadingCtrl: LoadingController, 
    private alertCtrl: AlertController,
    public accountService:AccountProvider
  ) {
  }

  // captchaResolved(response: any): void {
  //   console.log(response);
  //   this.zone.run(() => {
  //     this.captchaPassed = true;
  //     this.captchaResponse = response;
  //   });

  // }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goReg() {
    this.navCtrl.push(RegisterPage);
  }

  goTut() {
    this.navCtrl.push(IntroPage);
  }

  login(e, p) {
    this.accountService.login(e,p).subscribe(data => {
      console.log(data);
      if (data.response.token) {
        this.storage.set('user_info', data.user);
        this.storage.set('myToken', data.response.token);
        this.navCtrl.push(TabsPage);
      } else {
        let toast = this.toastCtrl.create({
          message: data.response,
          duration: 2000
        });
        toast.present();
      }
    },
      err => {
        let toast = this.toastCtrl.create({
          message: 'Error login: ' + err,
          duration: 3000
        });
        toast.present();
      });
  }
  forgotPassword() {
    //show alert
    const prompt = this.alertCtrl.create({
      title: 'نسيت كلمة المرور',
      message: "اكتب البريد الالكتروني المسجل به هذا الحساب",
      inputs: [
        {
          name: 'email',
          placeholder: 'البريد الالكتروني'
        },
      ],
      buttons: [
        {
          text: 'الغاء',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'ارسل',
          handler: data => {
            if (data) {
              let loading = this.loadingCtrl.create({
                content: '<img src="assets/imgs/preloader.gif"/>',
                spinner: 'hide',
                cssClass: 'transparent'
              })
              loading.present();
              this.accountService.resetPassword(data.email).subscribe(data => {
                console.log(data);
                loading.dismiss();
                if (data.error) {
                  let toast = this.toastCtrl.create({
                    duration: 2000,
                    message: data.error
                  })
                  toast.present();
                }
                if (data.success) {
                  let toast = this.toastCtrl.create({
                    duration: 2000,
                    message: data.success
                  })
                  toast.present();
                }
              }, err => {
                console.log(err);
                loading.dismiss();
                let toast = this.toastCtrl.create({
                  duration: 2000,
                  message: err
                })
                toast.present();
              })
            }
            console.log('submit clicked', data);
          }
        }
      ]
    });
    prompt.present();
  }


}
