import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { AccountProvider } from '../../providers/account/account';
/**
 * Generated class for the ConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html',
})
export class ConfirmPage {

  myEmail: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private http: Http,
    private loadingCtrl: LoadingController, 
    private storage: Storage, 
    private toastCtrl: ToastController,
    public accountService:AccountProvider
  ) {
    this.myEmail = this.navParams.get('email');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmPage');
  }
  submitCode(sms) {
    let loading = this.loadingCtrl.create({
      content: '<img src="assets/imgs/preloader.gif"/>',
      spinner: 'hide',
      cssClass: 'transparent'
    });
    loading.present();
    
    this.accountService.verifyAccount(sms,this.myEmail).subscribe((data: any) => {
      console.log(data);
      loading.dismiss();
      if (data.response) {
        this.storage.set('user_info', data.user);
        this.storage.set('myToken', data.response.token);
        let toast = this.toastCtrl.create({
          message: 'تم تأكيد حسابك',
          duration: 2000
        });
        toast.present().then(() => {
          this.navCtrl.push(TabsPage);
        })
      } else {
        let toast = this.toastCtrl.create({
          message: data.error,
          duration: 2000
        });
        toast.present();
      }
    }, err => {
      loading.dismiss();
      let toast = this.toastCtrl.create({
        message: 'لا يمكن تأكيد الرقم حاليا',
        duration: 2000
      });
      toast.present();
      console.log(err);
    })
  }

}
