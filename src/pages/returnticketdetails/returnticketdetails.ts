import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { BookingPage } from '../booking/booking';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/**
 * Generated class for the ReturnticketdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-returnticketdetails',
  templateUrl: 'returnticketdetails.html',
})
export class ReturnticketdetailsPage {
  myParsedRound: any;
  prevView: any;
  myOrderId: any;
  reserveData: any;
  myUserInfo: any;
  product: any;
  products: any;
  myMaxTickets: any;
  myToken: any;
  myRoundTripSeats: string;
  myStringGoSeats: string;
  myStringSeats: string;
  myUpdatedArray: any;
  mySeatDetails: any[];
  myTripDetails: any;
  myGo: any;
  myBoth: any;
  myNewBus: any;
  chart: any;
  mySeats: any[];
  trip: any;
  myBus: ({ number: number; color: string; } | { number: string; color: string; })[];
  backseats: any[];
  total: number = 0;
  tickets: number = 5;
  showPrice: boolean = false;
  bus: any[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private toast: ToastController, 
    private http: Http, 
    private storage: Storage, 
    private loadingCtrl: LoadingController
  ) {
    this.products = [];
    this.product = {};
    this.mySeats = [];
    this.myNewBus = [];
    this.chart = [];
    this.myBoth = [];
    this.mySeatDetails = [];
    this.myUpdatedArray = [];
    this.trip = this.navParams.get('trip');
    this.chart = this.trip.chartArray;
    this.storage.get('user_info').then((val) => {
      console.log(val);
      this.myUserInfo = val;
    });
    this.storage.get('max_tickets').then((val) => {
      console.log(val);
      this.myMaxTickets = val;
    });
    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }
    })

    //draw bus seats
    for (let x = 0; x < this.trip.chartArray.length; x++) {
      if (this.chart[x].class !== 'End of Row') {
        this.chart[x]['id'] = this.trip.id;
        this.chart[x]['city_to'] = this.trip.city_to;
        this.chart[x]['tripTravelDate'] = this.trip.tripTravelDate;
        this.chart[x]['trip_id'] = this.trip.trip_id;
        //repeat assigning keys for fawry endpoint
        this.chart[x]['boardingPoint'] = this.trip.id;
        this.chart[x]['travelTo'] = this.trip.city_to;
        this.chart[x]['travelDate'] = this.trip.tripTravelDate;
        this.chart[x]['seatNumber'] = this.chart[x]['seatNo'];
        this.myNewBus.push(this.chart[x]);
      }
    }
    console.log(this.myNewBus);

    this.storage.get('myGo').then((val) => {
      console.log(val);
      if (val) {
        this.myGo = val;
      }
    })
  }

  ionViewDidEnter() {
    console.log('entered');
    this.prevView = this.navCtrl.last().component.name;
    console.log(this.prevView);
    this.mySeats = [];
    this.products = [];
    this.product = {};
    this.total = 0;
    this.showPrice = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReturnticketdetailsPage');
  }

  book() {
    this.myStringSeats = JSON.stringify(this.mySeats);
    this.myStringGoSeats = JSON.stringify(this.myGo);
    this.myRoundTripSeats = JSON.stringify(this.mySeats.concat(this.myGo));
    this.myParsedRound = JSON.parse(this.myRoundTripSeats);
    console.log('my string seats', this.myStringSeats);
    console.log('my round trip seats', this.myRoundTripSeats);
    let loading = this.loadingCtrl.create({
      content: '<img src="assets/imgs/preloader.gif"/>',
      spinner: 'hide',
      cssClass: 'transparent'
    })
    loading.present();

    //call api to check for availability
    this.http.get('http://dev.tz17.info/public/api/check/' + this.myRoundTripSeats).map(res => res.json()).subscribe((data: any) => {
      console.log(data);
      if (data.availability === 1) {
        console.log('available');
        loading.dismiss();
        this.chargeFawry();
      } else {
        console.log('not available');
        loading.dismiss();
      }
    }, err => {
      console.log(err);
      loading.dismiss();
    })
  }

  //pick seats
  choose(s) {
    if (this.prevView == 'BookingPage') {
      if (s.color == 'green' && !this.total) {
        s.color = 'primary';
      }
    }
    if (s.color == 'primary') {
      let bookedtoast = this.toast.create({
        message: 'هذا الكرسي محجوز بالفعل',
        position: 'top',
        duration: 2000
      })
      bookedtoast.present();
    } else if (s.color == 'green') {
      s.color = 'gold';
      this.mySeats.splice(this.mySeats.indexOf(s.number), 1);
      this.total -= this.trip.ticket_price;
    } else if (s.color == 'light') {
      s.color = 'light';
    } else if (s.color == 'grey') {
      s.color = 'grey';
    } else {
      if (this.mySeats.length >= this.myMaxTickets) {
        let toast = this.toast.create({
          message: 'لا يمكنك حجز اكثر من ذلك',
          position: 'top',
          duration: 2000
        })
        toast.present();
      } else {
        s.color = 'green';
        this.mySeats.push(s);
        console.log(this.mySeats);
        this.total += this.trip.ticket_price;
      }
      this.showPrice = true;

    }

  }

  //go to the next screen where we'll use Fawry api
  chargeFawry() {
    var today = new Date();
    var dd: any = today.getDate();
    var mm: any = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    var myCurrentDate = yyyy + '-' + mm + '-' + dd;
    this.product.price = this.trip.ticket_price;
    this.product.quantity = this.mySeats.length;
    this.products.push(this.product);
    console.log(this.products);
    this.navCtrl.push(BookingPage, {
      date: myCurrentDate,
      products: this.products,
      myStringSeats: this.myRoundTripSeats,
      totalReturn: this.total,
      no_of_tickets: this.mySeats.length,
    });
  }
}
