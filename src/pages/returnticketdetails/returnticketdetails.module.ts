import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReturnticketdetailsPage } from './returnticketdetails';

@NgModule({
  declarations: [
    ReturnticketdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ReturnticketdetailsPage),
  ],
})
export class ReturnticketdetailsPageModule {}
