import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {

  myInfo: any={};
  no_of_tickets: any;
  payment: any;
  booking_date: any;
  myGo: any;
  myToken: any;
  myReturnTrip: any;
  myGoTrip: any={
    companyDetails:[
      {official_name:''}
    ]
  };
  data: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public storage:Storage,
    public http:Http
  ) {
    this.booking_date=this.navParams.get('booking_date');
    this.payment=this.navParams.get('payment');
    this.no_of_tickets=this.navParams.get('no_of_tickets');
    // this.data=this.navParams.get('data');
    this.storage.get('user_info').then((val) => {
      console.log(val);
      if (val) {
        this.myInfo = val;
      }
    });
    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }
    });
    this.storage.get('myGoFullTrip').then((val) => {
      console.log(val);
      if (val) {
        this.myGoTrip = val;
      }
    })
    this.storage.get('myGo').then((val) => {
      console.log(val);
      if (val) {
        this.myGo = val;
      }
    })
    this.storage.get('myReturnFullTrip').then((val) => {
      console.log(val);
      if (val) {
        this.myReturnTrip = val;
      }
    })  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }
  goHome(){
    this.navCtrl.setRoot(TabsPage);
  }
}
