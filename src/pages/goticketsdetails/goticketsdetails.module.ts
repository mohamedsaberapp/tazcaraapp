import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoticketsdetailsPage } from './goticketsdetails';

@NgModule({
  declarations: [
    GoticketsdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GoticketsdetailsPage),
  ],
})
export class GoticketsdetailsPageModule {}
