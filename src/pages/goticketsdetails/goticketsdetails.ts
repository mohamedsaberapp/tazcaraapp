import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ReturnticketsPage } from '../returntickets/returntickets';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the GoticketsdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-goticketsdetails',
  templateUrl: 'goticketsdetails.html',
})
export class GoticketsdetailsPage {
  myMaxTickets: any;
  myWholeTrip: any[];
  new: any[];
  neww: any[];
  myNewBus: any;
  chart: any;
  mySeats: any[];
  trip: any;
  myBus: ({ number: number; color: string; } | { number: string; color: string; })[];
  backseats: any[];
  total: number = 0;
  tickets: number = 5;
  showPrice: boolean = false;
  bus: any[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toast: ToastController,
    private storage: Storage
  ) {
    this.mySeats = [];
    this.myNewBus = [];
    this.neww = [];
    this.chart = [];

    this.storage.get('max_tickets').then((val) => {
      console.log(val);
      this.myMaxTickets = val;
    });
    this.storage.get('myReturnDetails')
      .then(
        data => {
          console.log(data);
        },
        error => console.error(error)
      );
    this.trip = this.navParams.get('trip');
    console.log(this.trip);

    //draw bus seats
    this.chart = this.trip.chartArray;
    for (let x = 0; x < this.trip.chartArray.length; x++) {
      if (this.chart[x].class !== 'End of Row') {
        this.chart[x]['id'] = this.trip.id;
        this.chart[x]['city_to'] = this.trip.city_to;
        this.chart[x]['tripTravelDate'] = this.trip.tripTravelDate;
        this.chart[x]['trip_id'] = this.trip.trip_id;
        //repeat assigning keys for fawry endpoint
        this.chart[x]['boardingPoint'] = this.trip.id;
        this.chart[x]['travelTo'] = this.trip.city_to;
        this.chart[x]['travelDate'] = this.trip.tripTravelDate;
        this.chart[x]['seatNumber'] = this.chart[x]['seatNo'];
        this.myNewBus.push(this.chart[x]);
      }
    }
    console.log(this.myNewBus);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoticketsdetailsPage');
  }
  book() {
    this.navCtrl.push(ReturnticketsPage, { trip: this.trip });
    console.log(this.mySeats);

    //store go tickets seats & trip details
    this.storage.set('myGo', this.mySeats).then(() => {
      this.storage.get('myGo').then((val) => {
        console.log(val);
      })
    })
    //store total go trip
    this.storage.set('totalmyGo', this.total).then(() => {
      this.storage.get('totalmyGo').then((val) => {
        console.log(val);
      })
    })

  }

  //pick a seat
  choose(s) {
    if (s.color == 'primary') {
      let bookedtoast = this.toast.create({
        message: 'هذا الكرسي محجوز بالفعل',
        position: 'top',
        duration: 2000
      })
      bookedtoast.present();
    } else if (s.color == 'green') {
      s.color = 'gold';
      this.mySeats.splice(this.mySeats.indexOf(s.number), 1);
      this.total -= this.trip.ticket_price;
    } else if (s.color == 'light') {
      s.color = 'light';
    } else if (s.color == 'grey') {
      s.color = 'grey';
    } else {
      if (this.mySeats.length >= this.myMaxTickets) {
        let toast = this.toast.create({
          message: 'لا يمكنك حجز اكثر من ذلك',
          position: 'top',
          duration: 2000
        })
        toast.present();
      } else {
        s.color = 'green';
        this.mySeats.push(s);
        console.log(this.mySeats);
        this.total += this.trip.ticket_price;
      }
      this.showPrice = true;
    }

  }

}
