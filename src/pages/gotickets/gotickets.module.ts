import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoticketsPage } from './gotickets';

@NgModule({
  declarations: [
    GoticketsPage,
  ],
  imports: [
    IonicPageModule.forChild(GoticketsPage),
  ],
})
export class GoticketsPageModule {}
