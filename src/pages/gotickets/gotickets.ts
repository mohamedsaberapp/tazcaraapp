import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, LoadingController } from 'ionic-angular';
import { GoticketsdetailsPage } from '../goticketsdetails/goticketsdetails';
import { FilterPage } from '../filter/filter';
import { Storage } from '@ionic/storage';
import { GetticketsProvider } from '../../providers/gettickets/gettickets';

/**
 * Generated class for the GoticketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gotickets',
  templateUrl: 'gotickets.html',
})
export class GoticketsPage {
  initTrips: any;
  returnDate:any;
  myPrevDay: any;
  myNextDay: any;
  prevView: any;
  city_to: any;
  city_from: any;
  from: any;
  to: any;
  goDate: any;
  myTrips: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private storage: Storage,
    public getticketsProvider: GetticketsProvider,
    private loading: LoadingController
  ) {

    this.myTrips = this.navParams.get('trips').Trips.trips;
    this.initTrips = this.myTrips;
    this.city_from = this.myTrips[0].city_from;
    this.city_to = this.myTrips[0].city_to;

    //store some info for the ucoming views
    this.storage.set('city_from_id', this.myTrips[0].city_from);
    this.storage.set('city_to_id', this.myTrips[0].city_to);
    this.storage.set('city_ids', this.myTrips[0].city_from + '&' + this.myTrips[0].city_to);
    this.storage.set('city_start_date', this.myTrips[0].tripTravelDate);
    this.goDate = this.navParams.get('start_date');
    this.returnDate = this.navParams.get('return_date');
    this.storage.set('city_return_date', this.returnDate);
    console.log(this.myTrips);

  }
  ionViewDidEnter() {
    //check the previous view and handle if the user clicked on the back button
    let previousView: ViewController = this.navCtrl.last();
    console.log(previousView.component.name);
    if (previousView.component.name == 'GoticketsdetailsPage') {
      this.storage.get('city_ids').then((val) => {
        console.log(val);
        if (val) {
          let loading = this.loading.create({
            content: '<img src="assets/imgs/preloader.gif"/>',
            spinner: 'hide',
            cssClass: 'transparent'
          });
          loading.present();
          this.city_from = val.split('&')[0];
          this.city_to = val.split('&')[1];
          this.getticketsProvider.getTickets(
            this.goDate,
            this.city_from,
            this.city_to
          ).subscribe((data: any) => {
            console.log(data);
            if (data.Trips.trips) {
              this.myTrips = data.Trips.trips
            }
            loading.dismiss();
          })
        } else {
          console.log('no saved ids cuz of empty value for city_ids');
        }
      })
    } else {
      console.log('no refresh needed cuz the previous view is ', previousView.component.name);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoticketsPage');
  }

  goDetails(trip) {
    this.navCtrl.push(GoticketsdetailsPage, {
      trip: trip
    });
    //store the information and data of the go trip to use in the upcoming views
    this.storage.set('choosedGoDate', this.goDate);
    this.storage.set('myGoFullTrip', trip).then(() => {
      this.storage.get('myGoFullTrip').then((val) => {
        console.log(val);
      })
    })
  }

  //filter tickets
  filter() {
    let modal = this.modalCtrl.create(FilterPage, { data: this.initTrips });
    modal.onDidDismiss(data => {
      if (data) {
        this.myTrips = data;
      }
      console.log('Filter Data :', data);
    })
    modal.present();
  }

  //show tickets of the next day
  goNextDay() {
    let nextDate = new Date(this.goDate);
    nextDate.setDate(nextDate.getDate() + 1);
    console.log(nextDate);
    this.myNextDay = nextDate.toISOString().split('T')[0];
    this.goDate = this.myNextDay;
    console.log('final date', this.myNextDay);
    let loading = this.loading.create({ content: '' });
    loading.present();
    this.getticketsProvider.getTickets(
      this.myNextDay, 
      this.city_from, 
      this.city_to
    ).subscribe((data: any) => {
      console.log(data);
      if (data.Trips.trips) {
        this.myTrips = data.Trips.trips;
      }
      loading.dismiss();
    })
  }

  //show tickets of the previous day
  goPrevDay() {
    let prevDate = new Date(this.goDate);
    prevDate.setDate(prevDate.getDate() - 1);
    console.log(prevDate);
    this.myPrevDay = prevDate.toISOString().split('T')[0];
    this.goDate = this.myPrevDay;
    console.log('final date', this.myPrevDay);
    let loading = this.loading.create({ content: '' });
    loading.present();
    this.getticketsProvider.getTickets(
      this.myPrevDay, 
      this.city_from, 
      this.city_to
    ).subscribe((data: any) => {
      console.log(data);
      if (data.Trips.trips) {
        this.myTrips = data.Trips.trips;
      }
      loading.dismiss();
    })
  }

}
