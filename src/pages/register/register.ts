import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { IntroPage } from '../intro/intro';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfirmPage } from '../confirm/confirm';
import { AccountProvider } from '../../providers/account/account';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  femaleChoice: string;
  maleChoice: string;
  gender: string;
  genderChoice: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private toastCtrl: ToastController, 
    public accountService:AccountProvider
  ) {
  }

  // captchaResolved(response: any): void {
  //   console.log(response);
  //   this.zone.run(() => {
  //     this.captchaPassed = true;
  //     this.captchaResponse = response;
  //   });

  // }

  male() {
    this.gender = "male";
    this.maleChoice = "secondary";
    this.femaleChoice = "grey";
  }

  female() {
    this.gender = "female";
    this.maleChoice = "grey";
    this.femaleChoice = "secondary";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  goLogin() {
    this.navCtrl.push(LoginPage);
  }

  goTut() {
    this.navCtrl.push(IntroPage);
  }

  register(n,p,dob,e,password,password_c,g) {
    this.accountService.register(n,p,dob,e,password,password_c,g).subscribe(data => {
        console.log(data);
        if(data.success){
          let toast = this.toastCtrl.create({
            message: 'تم ارسال طلبك',
            duration: 2000
          });
          toast.present();
          this.navCtrl.push(ConfirmPage,{email:e});
        }else{
          let toast = this.toastCtrl.create({
            message: 'تأكد من صحة بياناتك',
            duration: 2000
          });
          toast.present();
        }
        
      },
        err => {
          let toast = this.toastCtrl.create({
            message: ' خطأ في التسجيل : ' + err,
            duration: 3000
          });
          toast.present();
        });
  }
}
