import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { LoginPage } from '../login/login';
import { AccountProvider } from '../../providers/account/account';
/**
 * Generated class for the MytripsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mytrips',
  templateUrl: 'mytrips.html',
})
export class MytripsPage {
  myPendTrips: any;
  myPastTrips: any[];
  myNextTrips: any[];
  myToken: any;
  type: string = "next";
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private app: App,
    public storage: Storage, 
    public http: Http, 
    private loadingCtrl: LoadingController, 
    private toastCtrl: ToastController,
    public accountService:AccountProvider
  ) {

    this.type = 'next';
    this.myNextTrips = [];
    this.myPastTrips = [];
    this.myPendTrips = [];
    this.storage.get('myToken').then((val) => {
      console.log(val);
      if (val) {
        this.myToken = val;
      }
    })
  }

  ionViewDidEnter() {
    if (this.myToken) {
      let loading = this.loadingCtrl.create({
        content: '<img src="assets/imgs/preloader.gif"/>',
          spinner: 'hide',
          cssClass:'transparent'
      });
      loading.present();
      console.log('ionViewDidLoad MytripsPage');
      this.accountService.getProfile(this.myToken).subscribe(data => {
        console.log(data);
        loading.dismiss();
        if (data.nextTickets) {
          this.myNextTrips = data.nextTickets;
        }
        if (data.pastTickets) {
          this.myPastTrips = data.pastTickets;
        }
        if (data.pandingTickets) {
          this.myPendTrips = data.pandingTickets;
        }
      }, err => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'خطأ !',
          duration: 2000
        });
        toast.present();
        console.log(err);
      })
    }
  }
  
  goLog() {
    this.app.getRootNav().setRoot(LoginPage);
  }

  doRefresh(refresher){
    if (this.myToken) {
      let loading = this.loadingCtrl.create({
        content: '<img src="assets/imgs/preloader.gif"/>',
          spinner: 'hide',
          cssClass:'transparent'
      });
      loading.present();
      console.log('ionViewDidLoad MytripsPage');
      this.accountService.getProfile(this.myToken).subscribe(data => {
        console.log(data);
        loading.dismiss();
        refresher.complete();
        if (data.nextTickets) {
          this.myNextTrips = data.nextTickets;
        }
        if (data.pastTickets) {
          this.myPastTrips = data.pastTickets;
        }
        if (data.pandingTickets) {
          this.myPendTrips = data.pandingTickets;
        }
      }, err => {
        loading.dismiss();
        refresher.complete();
        let toast = this.toastCtrl.create({
          message: 'خطأ !',
          duration: 2000
        });
        toast.present();
        console.log(err);
      })
    }
  }
}
